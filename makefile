gccV := $(shell gcc -dumpversion)

vector:
	g++ vector.cpp -o fail

install: vector vector.cpp vector.hpp
	mkdir -p /usr/include/c++/$(gccV)/extended
	cp vector /usr/include/c++/$(gccV)/extended
	chmod 0755 /usr/include/c++/$(gccV)/extended/vector
	cp vector.hpp /usr/include/c++/$(gccV)/extended
	chmod 0755 /usr/include/c++/$(gccV)/extended/vector.hpp
	cp vector.cpp /usr/include/c++/$(gccV)/extended
	chmod 0755 /usr/include/c++/$(gccV)/extended/vector.cpp
